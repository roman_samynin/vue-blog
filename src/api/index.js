import request from './request'
const limitPosts = import.meta.env.VITE_APP_API_BASE_URL

  const getPostsByPage = async (page) => {
    const response = await request.get("/posts", { params: {_page:page,_limit:limitPosts }});
    return response;
  };

  const getPostsByText = async (text, page) => {
    const response = await request.get('/posts', { params: { q: text, _page: page,_limit: limitPosts } });
    return response;
  };

  const getPostsById = async (id) => {
    const response = await request.get("/posts", { params: {id} });
    return response;
  };

export { getPostsByPage, getPostsById, getPostsByText }