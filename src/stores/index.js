import { ref } from 'vue';
import { defineStore } from 'pinia';
import { getPostsByPage, getPostsByText, getPostsById } from "../api";

export const useApiStore = defineStore('ApiStore', () => {
  const isLoading = ref(false);

  const setLoading = (payload) => {
    isLoading.value = payload === 'on'
  };

  const getLoadingStatus = () => {
    return isLoading.value;
  };

  const getPostsPagination = async (page) => {
    const response = await getPostsByPage(page);
    return response;
  };

  const getPost = async (id) => {
    const response = await getPostsById(id);
    return response;
  };

  const searchPosts = async (text, page) => {
    const response = await getPostsByText(text, page);
    return response;
  };

  return { getLoadingStatus, setLoading, searchPosts, getPost, getPostsPagination };
})
